
# Install
* `brew install libsass`
* `npm install`
* `./node_modules/gulp/bin/gulp.js`
* `hugo -b "/"`
* `cd public && s3cmd put -P -M --recursive ./ s3://www.farm.red`
  * `s3cmd put -P --guess-mime-type --recursive ./ s3://www.farm.red`

# Testing
* `hugo server --config config.toml`

# TODO
* series section
* integration with commerce / stripe?

# Image processing
* crops are made at 120 px width, with `_tn` and are pngs
